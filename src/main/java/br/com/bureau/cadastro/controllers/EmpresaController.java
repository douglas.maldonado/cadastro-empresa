package br.com.bureau.cadastro.controllers;

import br.com.bureau.cadastro.models.Empresa;
import br.com.bureau.cadastro.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa createEmpresa(@RequestBody Empresa empresa){

        Empresa empresaObjeto = empresaService.createEmpresa(empresa);

        return empresaObjeto;
    }
}
