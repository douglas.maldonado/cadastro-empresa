package br.com.bureau.cadastro.services;

import br.com.bureau.cadastro.models.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public Empresa createEmpresa(Empresa empresa){

        sendKafka(empresa);

        return empresa;
    }

    public void sendKafka(Empresa empresa){
        producer.send("spec2-douglas-maldonado-2", empresa);
    }
}
